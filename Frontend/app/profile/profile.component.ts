import {Component} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'app-profile',
    templateUrl: './app/profile/profile.component.html',
})
export class ProfileComponent {

    user;

    name : string;
    surname : string;
    profilePicture : string;

    constructor(public http: Http) {
        this.showProfile();
        console.log(this.user);
    }

    showProfile() {
        this.http.get('http://localhost:8080/profile/' + sessionStorage.getItem("username"))
            .map(res => res.json())
            .subscribe(
                data => this.user = data,
                err => this.logError(err),
                () => console.log(this.user)
            );
    }

    updateProfile() {
        if (this.name == null) {
            this.name = this.user.name;
        }
        if (this.surname == null) {
            this.surname = this.user.surname;
        }
        if (this.profilePicture == null) {
            this.profilePicture = this.user.profilePicture;
        }
        let username = sessionStorage.getItem("username");
        if(username != null) {
            this.http.post('http://localhost:8080/profile/update/' + username + '/' + this.name + '/' +
                this.surname, this.profilePicture, {
            })
                .map(res => res.text())
                .subscribe(
                    err => this.logError(err),
                    () => this.showProfile()
                );
        }
    }

    logError(err) {
        console.error('There was an error: ' + err);
    }

}