import {Component} from '@angular/core';
import {Http} from "@angular/http";
import 'rxjs/Rx';

@Component({
    selector: 'app-home',
    templateUrl: './app/home/home.component.html'
})
export class HomeComponent {

    success : any;
    title : string;
    content : string;
    page : number;
    pageCount : number;
    news: { id: number, title: string, content : string, userId: number }[];

    decreasePage() {
        if(this.page > 0) {
            this.page--;
            this.showPageContent();
        }
    }

    increasePage() {
        console.log(this.page + 1 + " " + this.pageCount);
        if(this.page  < this.pageCount) {
            this.page++;
            this.showPageContent();
        }
    }

    showPageContent() {
        this.http.get('http://localhost:8080/news/' + this.page)
            .map(res => res.json())
            .subscribe(
                data => this.news = data,
                err => this.logError(err),
                () => console.log('New page')
            );
    }

    createNews() {
        this.http.post('http://localhost:8080/news/create/'  + sessionStorage.getItem("username") + "/" + this.title + "/" + this.content, {
        })
            .map(res => res.text())
            .subscribe(
                data => this.success = data,
                err => this.logError(err),
                () => this.showPageContent()
            );
    }

    constructor(public http: Http) {
        this.http.get('http://localhost:8080/news')
            .map(res => parseInt(res.text(), 10))
            .subscribe(
                data => this.pageCount = data,
                err => this.logError(err),
                () => console.log('Page count done.')
            );
        this.page = 0;
        this.http.get('http://localhost:8080/news/' + this.page)
            .map(res => res.json())
            .subscribe(
                data => this.news = data,
                err => this.logError(err),
                () => console.log('Random Quote Complete')
            );
    }

    logError(err) {
        console.error('There was an error: ' + err);
    }
};