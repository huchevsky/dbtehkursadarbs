///<reference path="../../node_modules/@angular/http/src/http.d.ts"/>
import {Component} from '@angular/core';
import {Http, Headers} from "@angular/http";

@Component({
    selector: 'app-registration',
    templateUrl: './app/registration/registration.component.html',
})
export class RegistrationComponent {
    username : string;
    password : string;
    name : string;
    surname : string;

    success : string;

    constructor(public http: Http){
    };

    register() {
        console.log("happened");

        this.http.post('http://localhost:8080/register/' + this.username + "/" + this.password + "/" + this.name + "/" + this.surname, {
        })
            .map(res => res.text())
            .subscribe(
                data => this.success = data,
                err => this.logError(err),
                () => console.log(this.success)
            );
    }

    logError(err) {
        console.error('There was an error: ' + err);
    }
};