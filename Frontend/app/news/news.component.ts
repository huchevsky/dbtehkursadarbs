import {Component} from '@angular/core';
import {Http} from "@angular/http";
import 'rxjs/Rx';
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-news',
    templateUrl: './app/news/news.component.html'
})
export class NewsComponent {

    success : string;
    newsId : number;
    comment : string;
    new: { title: string, content : string, authorName: string, authorSurname : string, dateCreated : string }[];
    comments: { name: string, surname : string, content : string }[];

    constructor(public http: Http, private route:ActivatedRoute) {
    }

    ngOnInit() {
        this.newsId = this.route.snapshot.params['id'];
        this.showNews();
        this.showComments();
    }


    showNews() {
        this.http.get('http://localhost:8080/news/read/' + this.newsId)
            .map(res => res.json())
            .subscribe(
                data => this.new = data,
                err => this.logError(err),
                () => console.log('New page')
            );
    }

    showComments() {
        this.http.get('http://localhost:8080/news/read/comments/' + this.newsId)
            .map(res => res.json())
            .subscribe(
                data => this.comments = data,
                err => this.logError(err),
                () => console.log('New page')
            );
    }

    ///news/add/comments/{newsId}/{username}/{content}

    addComment() {
        console.log(this.comment);
        this.http.post('http://localhost:8080/news/add/comments/' + this.newsId + "/" + sessionStorage.getItem("username") + "/" + this.comment, {
        })
            .map(res => res.text())
            .subscribe(
                data => this.success = data,
                err => this.logError(err),
                () => this.showComments()
            );
    }

    logError(err) {
        console.error('There was an error: ' + err);
    }
}