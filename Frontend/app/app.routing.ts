import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component'; //import home components
import { AboutComponent } from './about/about.component'; //import about component
import { LoginComponent } from './login/login.component'; //import login component
import { RegistrationComponent } from './registration/registration.component';
import {TeamComponent} from "./team/team.component";
import {ProjectComponent} from "./project/project.component";
import {ProfileComponent} from "./profile/profile.component";
import {TeamPageComponent} from "./team/teampage/teampage.component";
import {NewsComponent} from "./news/news.component"; //import registration component

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'teams', component: TeamComponent },
  { path: 'team/:teamId', component: TeamPageComponent },
  { path: 'team/:teamId/project/:projectId', component: ProjectComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'news/read/:id', component: NewsComponent},
  { path: '', component: HomeComponent, pathMatch: 'full'} // redirect to home page on load
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
