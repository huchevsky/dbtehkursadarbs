import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Http} from "@angular/http";

@Component({
    selector: 'app-teampage',
    templateUrl: './app/team/teampage/teampage.component.html',
})
export class TeamPageComponent {
    
    id : number;
    projectName : string;
    projectDesc : string;
    usrName: string;
    team: {name: string, description: string}[];
    projects: {id: number, name: string, description: string}[];
    members: {name: string, surname: string, profilePicture: string}[];

    constructor(public http: Http, private route:ActivatedRoute) {
    }

    ngOnInit() {
        this.id = this.route.snapshot.params['teamId'];
        this.http.get('http://localhost:8080/team/' + this.id)
            .map(res => res.json())
            .subscribe(
                data => this.team = data,
                err => this.logError(err),
                () => console.log("Team info loaded")
            );
        this.showProjects();
        this.showMembers();
    }

    logError(err) {
        console.error('There was an error: ' + err);
    }

    createProject() {
        this.http.post('http://localhost:8080/team/create/project/' + this.id + "/" + this.projectName + "/" + this.projectDesc, {
        })
            .map(res => res.text())
            .subscribe(
                err => this.logError(err),
                () => this.showProjects()
            );
        this.showProjects();
    }

    showProjects() {
        this.http.get('http://localhost:8080/team/projects/' + this.id)
            .map(res => res.json())
            .subscribe(
                data => this.projects = data,
                err => this.logError(err),
                () => console.log("Projects loaded")
            );
    }

    showMembers() {
        this.http.get('http://localhost:8080/team/projects/members/' + this.id)
            .map(res => res.json())
            .subscribe(
                data => this.members = data,
                err => this.logError(err),
                () => console.log("Member list loaded")
            );
    }

    addMember() {
        this.http.post('http://localhost:8080/team/addMember/' + this.id + "/" + this.usrName, {
        })
            .map(res => res.text())
            .subscribe(
                err => this.logError(err),
                () => this.showMembers()
            );
    }

}