import {Component} from '@angular/core';
import {Http} from "@angular/http";

@Component({
    selector: 'app-team',
    templateUrl: './app/team/team.component.html',
})
export class TeamComponent {

    name : string;
    description : string;
    teams: {id: number, name: string, description: string }[];

    constructor(public http: Http) {
        this.showTeams();
    }

    createTeam() {
        let username = sessionStorage.getItem("username");
        if(username != null) {
            this.http.post('http://localhost:8080/team/create/' + this.name + '/' + this.description + '/' + username, {
            })
                .map(res => res.text())
                .subscribe(
                    err => this.logError(err),
                    () => this.showTeams()
                );
        }
    }

    showTeams() {
        this.http.get('http://localhost:8080/teams/' + sessionStorage.getItem("username"))
            .map(res => res.json())
            .subscribe(
                data => this.teams = data,
                err => this.logError(err),
                () => console.log("Teams loaded")
            );
    }

    logError(err) {
        console.error('There was an error: ' + err);
    }

}