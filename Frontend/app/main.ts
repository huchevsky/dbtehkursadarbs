import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';
import {HTTP_PROVIDERS} from "angular2/http";
import {ROUTER_DIRECTIVES} from "angular2/router";

const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule, [HTTP_PROVIDERS, ROUTER_DIRECTIVES]);