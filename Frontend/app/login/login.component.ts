import {Component} from '@angular/core';
import {Http} from "@angular/http";
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './app/login/login.component.html',
})
export class LoginComponent {

    username : string;
    password : string;
    response : number;

    constructor(public http: Http, private router: Router) {
    }

    login() {
        this.http.get('http://localhost:8080/login/' + this.username + "." + this.password)
            .map(res => parseInt(res.text(), 10))
            .subscribe(
                data => this.response = data,
                err => this.logError(err),
                () => console.log(this.response)
            );
        if(this.response != -1) {
            sessionStorage.setItem("username", this.username);
        }
    }

    logError(err) {
        console.error('There was an error: ' + err);
    }
};