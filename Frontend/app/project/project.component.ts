import {Component} from '@angular/core';
import {Http} from "@angular/http";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-project',
    templateUrl: './app/project/project.component.html',
})
export class ProjectComponent {

    teamId : number;
    projectId : number;
    project;

    closedStories = [];
    openStories = [];
    inProgress = [];
    
    
    storyName : string;
    storyDesc : string;


    taskName : string;
    taskDesc : string;
    taskinProgress : boolean;
    taskclosed : boolean;
    taskId : number;
    storyId : number;
    taskCategory : string;

    constructor(public http: Http, private route:ActivatedRoute) {
    }

    ngOnInit() {
        this.teamId = this.route.snapshot.params['teamId'];
        this.projectId = this.route.snapshot.params['projectId'];
        this.loadProject(this.teamId, this.projectId);
    }
    
    loadProject(teamId : number, projectId : number) {
        this.http.get('http://localhost:8080/team/project/' + projectId)
            .map(res => res.json())
            .subscribe(
                data => this.project = data,
                err => this.logError(err),
                () => this.sortStories()
            );
        //this.sortStories();
    }

    sortStories() {
        console.log(this.project.length);
        console.log(this.project);
        for (var i = 0; i < this.project.length; i++) {
            console.log(this.project[i].closed);
            if (this.project[i].closed == false) {
                for (var j = 0; j < this.project[i].tasks.length; j++) {
                    if (this.project[i].tasks[j].inProgress == true) {
                        this.inProgress.push(this.project[i]);
                    }
                }
                console.log("happened");
                this.openStories.push(this.project[i]);
            } else {
                this.closedStories.push(this.project[i]);
            }
        }
    }

    findOpenTasks(story) {
        var tasks = [];
        for (var i = 0; i < story.tasks.length; i++) {
            if (story.tasks[i].closed == false && story.tasks[i].inProgress == false) {
                tasks.push(story.tasks[i]);
            }
        }
        return tasks;
    }

    findInProgressTasks(story) {
        var tasks = [];
        for (var i = 0; i < story.tasks.length; i++) {
            if (story.tasks[i].closed == false && story.tasks[i].inProgress == true) {
                tasks.push(story.tasks[i]);
            }
        }
        return tasks;
    }

    findClosedTasks(story) {
        var tasks = [];
        for (var i = 0; i < story.tasks.length; i++) {
            if (story.tasks[i].closed == true) {
                tasks.push(story.tasks[i]);
            }
        }
        return tasks;
    }

    taskInfo(task) {
        this.taskName = task.name;
        this.taskDesc = task.description;
        this.taskclosed = task.closed;
        this.taskinProgress = task.inProgress;
        this.taskCategory = task.category;
        this.taskId = task.taskId;
        this.storyId = task.storyId;
    }

    storyInfo(story) {
        this.storyId = story.id;
    }

    closeTask(task) {
        this.http.post('http://localhost:8080/team/project/' + this.projectId + '/' + task.storyId + '/' +
            task.taskId, {
        })
            .map(res => res.text())
            .subscribe(
                err => this.logError(err),
                () => this.loadProject(this.teamId, this.projectId)
            );
    }

    closeStory() {
        this.http.post('http://localhost:8080/team/project/' + this.projectId + '/' + this.storyId, {
        })
            .map(res => res.text())
            .subscribe(
                err => this.logError(err),
                () => this.loadProject(this.teamId, this.projectId)
            );
    }


    logError(err) {
        console.error('There was an error: ' + err);
    }

}