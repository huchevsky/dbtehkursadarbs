import { Component } from '@angular/core';


@Component({
    selector: 'app-root',
    templateUrl: './app/app.component.html',
})
export class AppComponent {

    isLoggedIn() {
        if(sessionStorage.getItem("username") != null) {
            return true;
        } else {
            return false;
        }
    }

    logout() {
        if(sessionStorage.getItem("username") != null) {
            sessionStorage.removeItem("username");
        }
    }
}