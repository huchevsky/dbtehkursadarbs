import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }   from './app.component';
import { HomeComponent } from './home/home.component'; //import home components
import { AboutComponent } from './about/about.component'; //import about component
import { LoginComponent } from './login/login.component'; //import login component
import { RegistrationComponent } from './registration/registration.component'; //import login component
import { routing }  from './app.routing';
import {HttpModule, JsonpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {TeamComponent} from "./team/team.component";
import {ProjectComponent} from "./project/project.component";
import {ProfileComponent} from "./profile/profile.component";
import {TeamPageComponent} from "./team/teampage/teampage.component";
import {NewsComponent} from "./news/news.component";

@NgModule({
  imports:      [ BrowserModule, routing, HttpModule,
    FormsModule,
    JsonpModule
  ], //other modules the app depends on
  declarations: [ AppComponent, AboutComponent, HomeComponent,
    LoginComponent, RegistrationComponent,
    TeamComponent, TeamPageComponent, ProjectComponent, ProfileComponent, NewsComponent ], // declare all derectives and components
  bootstrap : [ AppComponent ] // root component to bootstarp
})
export class AppModule { }