package com.dbteh.rest;

import com.dbteh.model.Project;
import com.dbteh.model.Team;
import com.dbteh.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by huch on 04.01.2017.
 */
@RestController
public class TeamController {

    @Autowired
    public DriverManagerDataSource source;

    @RequestMapping("/team/projects/members/{id}")
    public List<User> getTeammembers(@PathVariable int id) throws SQLException {
        List<User> members = new ArrayList<>();
        Connection c = DataSourceUtils.getConnection(source);
        try {
            PreparedStatement ps = c.prepareStatement(
                    "SELECT user.idUser as Id, user.Name as Name, user.Surname as Surname, user.ProfilePicture as Picture" +
                            " FROM user JOIN team_has_user on team_has_user.Team_idTeam = " + id +" WHERE team_has_user.User_idUser = user.idUser");
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                members.add(new User(rs.getInt("Id"), rs.getString("Name"), rs.getString("Surname"), rs.getString("Picture")));
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return members;
    }

    @RequestMapping("/team/addMember/{id}/{name}")
    public int addTeammember(@PathVariable int id, @PathVariable String name) throws SQLException {
        Connection c = DataSourceUtils.getConnection(source);
        try {
            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
            // retrieve a list of three random cities
            PreparedStatement ps = c.prepareStatement("INSERT INTO team_has_user (Team_idTeam, User_idUser) VALUES ('" + id + "', " +
                    "(SELECT idUser FROM user WHERE Username = '" + name +"'))");
            ps.executeUpdate();
            System.out.println("success");
            return 1;
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            c.rollback();
            // ignore failure closing connection
            try { c.rollback(); c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return -1;
    }

    @RequestMapping("/team/create/project/{id}/{name}/{description}")
    public int createProject(@PathVariable int id, @PathVariable String name, @PathVariable String description) throws SQLException {
        System.out.println("Create");
        Connection c = DataSourceUtils.getConnection(source);
        try {
            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
            // retrieve a list of three random cities
            PreparedStatement ps = c.prepareStatement("INSERT INTO project (Name, Description, Team_idTeam) VALUES ('" + name +"', '" + description+"', '" + id +"')");
            ps.executeUpdate();
            System.out.println("success");
            return 1;
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            c.rollback();
            // ignore failure closing connection
            try { c.rollback(); c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return -1;
    }

    @RequestMapping("/team/projects/{id}")
    public List<Project> showProjects(@PathVariable int id) throws SQLException {
        List<Project> projects = new ArrayList<>();
        Connection c = DataSourceUtils.getConnection(source);
        try {
            PreparedStatement ps = c.prepareStatement(
                    "SELECT idProject as 'Id', Name as 'Name', Description as 'Description' FROM project WHERE Team_idTeam = " + id)
                    ;
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                projects.add(new Project(rs.getInt("Id"), rs.getString("Name"), rs.getString("Description")));
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return projects;
    }

    @RequestMapping("/team/{id}")
    public Team getTeamInfo(@PathVariable int id) throws SQLException {

        Connection c = DataSourceUtils.getConnection(source);
        try {
            PreparedStatement ps = c.prepareStatement(
                    "SELECT Name as 'Name', Description as 'Description' FROM team " +
                            "WHERE idTeam = " + id)
                    ;
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                return new Team(id, rs.getString("Name"), rs.getString("Description"));
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return null;
    }

    @RequestMapping("/team/create/{name}/{description}/{username}")
    public int createTeam(@PathVariable String name, @PathVariable String description, @PathVariable String username) throws SQLException {
        System.out.println("Create");
        Connection c = DataSourceUtils.getConnection(source);
        try {
            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
            // retrieve a list of three random cities
            c.setAutoCommit(false);
            PreparedStatement ps = c.prepareStatement("INSERT INTO team (Name, Description, User_idUser) VALUES ('"+ name +"', '" + description + "'" +
                    ", (SELECT idUser FROM user WHERE Username like '" + username +"'))", Statement.RETURN_GENERATED_KEYS);
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            int lastid = keys.getInt(1);
//            PreparedStatement ps2 = c.prepareStatement("INSERT INTO user_has_team (User_idUser, Team_idTeam) VALUES (" +
//                            "(SELECT idUser FROM user WHERE Username like '" + username +"'), " + lastid + ");");
            PreparedStatement ps3 = c.prepareStatement("INSERT INTO team_has_user (User_idUser, Team_idTeam) VALUES (" +
                    "(SELECT idUser FROM user WHERE Username like '" + username +"'), " + lastid + ");");

            //ps2.executeUpdate();
            ps3.executeUpdate();
            c.commit();
            System.out.println("success");
            return 1;
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            c.rollback();
            // ignore failure closing connection
            try { c.rollback(); c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return -1;
    }

    @RequestMapping("/teams/{name}")
    public List<Team> getTeams(@PathVariable String name) {
        List<Team> teams = new ArrayList<>();
        Connection c = DataSourceUtils.getConnection(source);
        try {
            PreparedStatement ps = c.prepareStatement(
                    "SELECT team.idTeam as 'Id', team.Name as 'Name', team.description as 'Description' FROM team " +
                            "JOIN team_has_user on team_has_user.User_idUser = (SELECT idUser FROM user WHERE Username like '"+ name +"')" +
                            " WHERE team_has_user.Team_idTeam = team.idTeam")
                    ;
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                teams.add(new Team(rs.getInt("Id"), rs.getString("Name"), rs.getString("Description")));
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return teams;
    }
}
