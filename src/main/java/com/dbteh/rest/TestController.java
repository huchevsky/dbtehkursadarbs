package com.dbteh.rest;

import com.dbteh.dao.impl.UserDaoImpl;
import com.dbteh.model.User;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by huch on 10.12.2016.
 */
@RestController
public class TestController {

    @Autowired
    public DriverManagerDataSource source;

    @RequestMapping("/greeting")
    public User greeting(@RequestParam(value="name", defaultValue="World") String name) throws SQLException {
        System.out.println(source);
        System.out.println(source.getUsername());
        return new User("ubububu", "661331555", 0);
    }

    @RequestMapping("/test")
    public String yo() {
        Connection c = DataSourceUtils.getConnection(source);
        try {
            // retrieve a list of three random cities
            PreparedStatement ps = c.prepareStatement(
                    "select Content as 'Title' from news where idNews = 2");
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String city = rs.getString("Title");
                return city;
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return "";
    }
    @RequestMapping("/create")
    public String initialize() {
        UserDaoImpl users = new UserDaoImpl();
        users.init();
        return "seems ok";
    }

}
