package com.dbteh.rest;

import com.dbteh.model.Comment;
import com.dbteh.model.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by huch on 11.12.2016.
 */
@RestController
public class NewsController {

    @Autowired
    public DriverManagerDataSource source;

    @RequestMapping("/news/create/{username}/{title}/{content}")
    public boolean createNews(@PathVariable String username, @PathVariable String title, @PathVariable String content) {
        System.out.println("Add new comment on news id: " + username + " title " + title + "\n" + "Content : " + content);
        Connection c = DataSourceUtils.getConnection(source);
        try {
            PreparedStatement ps = c.prepareStatement(
                    "INSERT INTO news(Title, Content, User_idUser, DateCreated) " +
                            "VALUES ('" + title + "', '" + content + "', (SELECT idUser FROM user WHERE Username LIKE '" + username + "'), NOW())");
            int rows = ps.executeUpdate();
            if(rows == 1) {
                return true;
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        System.out.println("null");
        return false;
    }

    @RequestMapping("/news")
    public int getPageCount() {

        Connection c = DataSourceUtils.getConnection(source);
        try {
            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
            // retrieve a list of three random cities
            PreparedStatement ps = c.prepareStatement(
                    "select COUNT(idNews) as 'NewsCount' from news");
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                return rs.getInt("NewsCount") / 4;
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return 0;
    }

    @RequestMapping("/news/read/{id}")
    public News readNews(@PathVariable int id) {
        System.out.println("pls " + id);
        Connection c = DataSourceUtils.getConnection(source);
        try {
            PreparedStatement ps = c.prepareStatement(
                    "SELECT news.Title AS 'Title', news.Content AS 'Content', news.DateCreated AS 'Date', " +
                            "user.Name AS 'Name', user.Surname AS 'Surname' " +
                            "FROM news " +
                            "JOIN user on user.idUser = news.User_idUser WHERE news.idNews = " + id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String title = rs.getString("Title");
                String content = rs.getString("Content");
                String name = rs.getString("Name");
                System.out.println(name);
                String surname = rs.getString("Surname");
                System.out.println(surname);
                String date = rs.getString("Date");

                return new News(id, title, content, name, surname, date);
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        System.out.println("null");
        return null;
    }

    @RequestMapping("/news/read/comments/{id}")
    public List<Comment> readNewsComments(@PathVariable int id) {
        System.out.println("pls " + id);
        List<Comment> comments = new ArrayList<>();
        Connection c = DataSourceUtils.getConnection(source);
        try {
            PreparedStatement ps = c.prepareStatement(
                    "SELECT  user.Name AS 'Name', user.Surname AS 'Surname', " +
                            "comment.Content AS 'Content' " +
                            "FROM comment " +
                            "JOIN user on user.idUser = comment.User_idUser" +
                            " WHERE comment.News_idNews = " + id);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String name = rs.getString("Name");
                String surname = rs.getString("Surname");
                String content = rs.getString("Content");

                comments.add(new Comment(name, surname, content));
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        System.out.println("null");
        return comments;
    }

    @RequestMapping("/news/add/comments/{newsId}/{username}/{content}")
    public boolean addNewsComments(@PathVariable int newsId, @PathVariable String username, @PathVariable String content) {
        System.out.println("Add new comment on news id: " + newsId + " by user: " + username + "\n" + "Content : " + content);
        Connection c = DataSourceUtils.getConnection(source);
        try {
            PreparedStatement ps = c.prepareStatement(
                    "INSERT INTO comment(Content, News_idNews, User_idUser) " +
                            "VALUES ('" + content + "', '" + newsId + "', (SELECT idUser FROM user WHERE Username LIKE '" + username + "'))");
            int rows = ps.executeUpdate();
            if(rows == 1) {
                return true;
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        System.out.println("null");
        return false;
    }

    @RequestMapping("/news/{page}")
    public List<News> message(@PathVariable int page) {//REST Endpoint.
        System.out.println(page);
        System.out.println(page * 4);
        System.out.println(page * 4 + 5);
        List<News> news = new ArrayList<>();

        Connection c = DataSourceUtils.getConnection(source);
        try {
            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
            // PAGE ->
            // 6,5,4,3
            // PAGE 1 ->
            // 2,1
            // COUNT(idNews) - page * 4 < idNews && COUNT - 4 - page * 4 > idNews
            // retrieve a list of three random cities
            PreparedStatement ps = c.prepareStatement(
                    "select idNews as 'Id', Title as 'Title', " +
                            //"Content as 'Content' from news where COUNT(idNews) -" + page * 4 + " < idNews AND idNews < COUNT(idNews) - 4" + page * 4 + " ORDER BY IdNews ASC");
                            //"LEFT(Content , 150) as 'Content' from news where idNews > " + page * 4 + " AND idNews < " + page * 4 + 5 + " ORDER BY DATE(DateCreated) DESC");
                            "LEFT(Content , 150) as 'Content' from news ORDER BY UNIX_TIMESTAMP(DateCreated) DESC LIMIT " + page * 4 + ", "+ page * 4 + 4);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                int id = rs.getInt("Id");
                String title = rs.getString("Title");
                String content = rs.getString("Content");

                News entry = new News(id, title, content, 0);
                news.add(entry);

            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }

        return news;
    }

}
