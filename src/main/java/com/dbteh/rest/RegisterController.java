package com.dbteh.rest;

import com.dbteh.model.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by huch on 11.12.2016.
 */
@RestController
public class RegisterController {

    @Autowired
    public DriverManagerDataSource source;

    @RequestMapping("/register/{username}/{password}/{name}/{surname}")
    public boolean register(@PathVariable String username, @PathVariable String password, @PathVariable String name, @PathVariable String surname) {//REST Endpoint.
        System.out.println(username);
        System.out.println(password);
        System.out.println(name);
        System.out.println(surname);
        Connection c = DataSourceUtils.getConnection(source);
        try {
            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
            // retrieve a list of three random cities
            PreparedStatement ps = c.prepareStatement(
                    "INSERT INTO user(Username, Password, Name,Surname) " +
                            "VALUES ('" + username + "', '" + password + "', '" + name + "', '" + surname + "')");
            int rows = ps.executeUpdate();
            if(rows == 1) {
                return true;
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return false;
    }
}
