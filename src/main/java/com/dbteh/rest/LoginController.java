package com.dbteh.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by huch on 11.12.2016.
 */
@RestController
public class LoginController {

    @Autowired
    public DriverManagerDataSource source;

    @RequestMapping("/login/{username}.{password}")
    public int getLogin(@PathVariable String username, @PathVariable String password) {

        Connection c = DataSourceUtils.getConnection(source);
        try {
            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
            // retrieve a list of three random cities
            PreparedStatement ps = c.prepareStatement(
                    "select idUser as 'Id' from user WHERE Username LIKE '" + username + "' AND Password LIKE '" + password + "'");
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                return rs.getInt("Id");
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return -1;
    }

}
