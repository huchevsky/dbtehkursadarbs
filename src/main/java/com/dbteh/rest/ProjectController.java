package com.dbteh.rest;

import com.dbteh.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by huch on 12.01.2017.
 */
@RestController
public class ProjectController {

    @Autowired
    public DriverManagerDataSource source;

//    @RequestMapping("/team/project/{projectId}/{storyId}/{taskName}/{taskDesc}/taskCat")
//    public int createTask(@PathVariable int projectId, @PathVariable int storyId,
//                           @PathVariable String taskName, @PathVariable String taskDesc,
//                          @PathVariable String taskCat) throws SQLException {//REST Endpoint.
//        Connection c = DataSourceUtils.getConnection(source);
//        try {
//            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
//            // retrieve a list of three random cities
//            PreparedStatement ps = c.prepareStatement("INSERT INTO task (Name, Description, Category, Story_storyId, taskId, Closed) VALUES ('"
//                    + taskName + "', '" + taskDesc + "','" + taskCat + "','" + storyId + "', createTaskId('" +projectId +"', '" + storyId +"', 0)");
//            ps.executeUpdate();
//            System.out.println("success");
//            return 1;
//        } catch (SQLException ex) {
//            // something has failed and we print a stack trace to analyse the error
//            ex.printStackTrace();
//            c.rollback();
//            // ignore failure closing connection
//            try { c.rollback(); c.close(); } catch (SQLException e) { }
//        } finally {
//            // properly release our connection
//            DataSourceUtils.releaseConnection(c, source);
//        }
//        return -1;
//    }
//
    @RequestMapping("/team/project/{projectId}/{storyId}/{taskId}")
    public int closeTask(@PathVariable int projectId, @PathVariable int storyId, @PathVariable int taskId) throws SQLException {
        Connection c = DataSourceUtils.getConnection(source);
        try {
            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
            // retrieve a list of three random cities
            PreparedStatement ps = c.prepareStatement("UPDATE task SET task.Closed = 1 WHERE " +
                    "task.idTask = " + taskId);
            ps.executeUpdate();
            System.out.println("success");
            return 1;
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            c.rollback();
            // ignore failure closing connection
            try { c.rollback(); c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return -1;
    }

    @RequestMapping("/team/project/{projectId}/{storyId}")
    public int closeStory(@PathVariable int projectId, @PathVariable int storyId) throws SQLException {
        Connection c = DataSourceUtils.getConnection(source);
        try {
            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
            // retrieve a list of three random cities
            PreparedStatement ps = c.prepareStatement("UPDATE story SET story.Closed = 1 WHERE " +
                    "story.idStory = " + storyId);
            ps.executeUpdate();
            System.out.println("success");
            return 1;
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            c.rollback();
            // ignore failure closing connection
            try { c.rollback(); c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return -1;
    }

//    @RequestMapping("/team/project/{projectId}/{storyId}")
//    public int reopenStory(@PathVariable int projectId, @PathVariable int storyId) throws SQLException {
//        Connection c = DataSourceUtils.getConnection(source);
//        try {
//            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
//            // retrieve a list of three random cities
//            PreparedStatement ps = c.prepareStatement("UPDATE story SET story.Closed = 0 WHERE storyId = " + storyId);
//            ps.executeUpdate();
//            System.out.println("success");
//            return 1;
//        } catch (SQLException ex) {
//            // something has failed and we print a stack trace to analyse the error
//            ex.printStackTrace();
//            c.rollback();
//            // ignore failure closing connection
//            try { c.rollback(); c.close(); } catch (SQLException e) { }
//        } finally {
//            // properly release our connection
//            DataSourceUtils.releaseConnection(c, source);
//        }
//        return -1;
//    }
//
//    @RequestMapping("/team/project/{projectId}/{storyName}/{storyDesc}")
//    public int createStory(@PathVariable int projectId, @PathVariable String storyName,
//                           @PathVariable String storyDesc) throws SQLException {//REST Endpoint.
//        Connection c = DataSourceUtils.getConnection(source);
//        try {
//            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
//            // retrieve a list of three random cities
//            PreparedStatement ps = c.prepareStatement("INSERT INTO story (Name, Description, Project_idProject, Closed) VALUES ('"
//                    + storyName + "', '" + storyDesc + "','" + projectId + "', 0)");
//            ps.executeUpdate();
//            System.out.println("success");
//            return 1;
//        } catch (SQLException ex) {
//            // something has failed and we print a stack trace to analyse the error
//            ex.printStackTrace();
//            c.rollback();
//            // ignore failure closing connection
//            try { c.rollback(); c.close(); } catch (SQLException e) { }
//        } finally {
//            // properly release our connection
//            DataSourceUtils.releaseConnection(c, source);
//        }
//        return -1;
//    }

    @RequestMapping("/team/project/{projectId}")
    public List<Story> getProjectInfo(@PathVariable int projectId) {//REST Endpoint.
        List<Story> storyList = new ArrayList<>();
        Connection c = DataSourceUtils.getConnection(source);
        try {
            //Take all open stories
            PreparedStatement ps = c.prepareStatement(
                    "SELECT * FROM story_info " +
                            "WHERE story_info.Project_idProject = " + projectId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {

                storyList.add(new Story(rs.getInt("idStory"), rs.getString("Name"), rs.getString("Description"),
                        false, new ArrayList<Task>()));
            }
            for (Story s : storyList) {
                System.out.println("Story ID: " + s.getId());
                ps = c.prepareStatement(
                        "SELECT * FROM task_info " +
                                "WHERE task_info.Story_storyId = " + s.getId());
                rs = ps.executeQuery();
                while(rs.next()) {
                    s.getTasks().add(new Task(rs.getString("Name"), rs.getString("Description"),
                            rs.getString("Category"), rs.getInt("Story_storyId"),
                            rs.getBoolean("Closed"), rs.getInt("taskId"), rs.getBoolean("inProgress")));
                }
            }
            return storyList;
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return null;
    }
}
