package com.dbteh.rest;

import com.dbteh.model.Team;
import com.dbteh.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by huch on 12.12.2016.
 */
@RestController
public class ProfilController {

    @Autowired
    public DriverManagerDataSource source;

    @RequestMapping("/profile/{username}")
    public User getProfileInfo(@PathVariable String username) {//REST Endpoint.
        Connection c = DataSourceUtils.getConnection(source);
        try {
            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
            // retrieve a list of three random cities
            PreparedStatement ps = c.prepareStatement(
                    "SELECT * FROM user_info WHERE Username = '" + username + "'");
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                return new User(username, rs.getString("Name"), rs.getString("Surname"), rs.getString("ProfilePicture"));
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return null;
    }

    @RequestMapping(value = "/profile/update/{username}/{name}/{surname}", headers="Accept=application/x-www-form-urlencoded")
    public boolean updateProfile(@PathVariable String username, @PathVariable String name,
                                 @PathVariable String surname, @RequestBody String image) {//REST Endpoint.
        Connection c = DataSourceUtils.getConnection(source);
        try {
            //SELECT Title, Content FROM news WHERE id > page * 4 OR id < page * 4 + 4 ORDER BY id;
            // retrieve a list of three random cities
            PreparedStatement ps = c.prepareStatement(
                    "UPDATE user SET ProfilePicture='" + image + "', Name= '" + name + "', Surname = '" + surname + "' " +
                            "WHERE Username = '" + username + "'");
            int rows = ps.executeUpdate();
            if(rows == 1) {
                return true;
            }
        } catch (SQLException ex) {
            // something has failed and we print a stack trace to analyse the error
            ex.printStackTrace();
            // ignore failure closing connection
            try { c.close(); } catch (SQLException e) { }
        } finally {
            // properly release our connection
            DataSourceUtils.releaseConnection(c, source);
        }
        return false;
    }

}
