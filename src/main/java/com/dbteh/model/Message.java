package com.dbteh.model;

/**
 * Created by huch on 20.02.2017.
 */
public class Message {

    private String title;
    private String content;
    private String sender;
    private boolean read;


    public Message(String title, String content, String sender, boolean read) {
        this.title = title;
        this.content = content;
        this.sender = sender;
        this.read = read;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }
}
