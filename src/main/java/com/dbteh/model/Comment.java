package com.dbteh.model;

public class Comment {

    private String name, surname, content;

    public Comment(String name, String surname, String content) {
        this.name = name;
        this.surname = surname;
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
