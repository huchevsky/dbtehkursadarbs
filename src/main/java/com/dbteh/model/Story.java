package com.dbteh.model;

import java.util.List;

/**
 * Created by huch on 20.02.2017.
 */
public class Story {

    private List<Task> tasks;
    private String name;
    private String description;
    private boolean closed;
    private int id;


    public Story(int id, String name, String description, boolean closed, List<Task> tasks) {
        this.tasks = tasks;
        this.name = name;
        this.description = description;
        this.closed = closed;
        this.id = id;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
