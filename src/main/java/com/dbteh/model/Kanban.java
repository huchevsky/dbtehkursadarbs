package com.dbteh.model;

import java.util.List;

/**
 * Created by huch on 20.02.2017.
 */
public class Kanban {

    private List<Story> open;
    private List<Story> closed;

    public Kanban(List<Story> open, List<Story> closed) {
        this.open = open;
        this.closed = closed;
    }

    public List<Story> getOpen() {
        return open;
    }

    public void setOpen(List<Story> open) {
        this.open = open;
    }

    public List<Story> getClosed() {
        return closed;
    }

    public void setClosed(List<Story> closed) {
        this.closed = closed;
    }
}
