package com.dbteh.model;

/**
 * Created by huch on 11.12.2016.
 */
public class News {

    private int id;
    private String title;
    private String content;
    private int userId;
    private String userName;
    private String authorName;
    private String authorSurname;

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    private String dateCreated;

    public News(int id, String title, String content, int userId) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.userId = userId;
    }

    public News(int id, String title, String content, String userName) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.userName = userName;
    }

    public News(int id, String title, String content, String authorName, String authorSurname) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.authorName = authorName;
        this.authorSurname = authorSurname;
    }

    public News(int id, String title, String content, String authorName, String authorSurname, String dateCreated) {
        this(id, title, content, authorName, authorSurname);
        this.dateCreated = dateCreated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorSurname() {
        return authorSurname;
    }

    public void setAuthorSurname(String authorSurname) {
        this.authorSurname = authorSurname;
    }
}
