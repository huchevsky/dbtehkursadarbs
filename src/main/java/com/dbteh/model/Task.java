package com.dbteh.model;

/**
 * Created by huch on 20.02.2017.
 */
public class Task {

    private String name;
    private String description;
    private String category;
    private int storyId;
    boolean closed;
    private int taskId;
    private boolean inProgress;

    public Task(String name, String description, String category, int storyId, boolean closed, int taskId, boolean inProgress) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.storyId = storyId;
        this.closed = closed;
        this.taskId = taskId;
        this.inProgress = inProgress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getStoryId() {
        return storyId;
    }

    public void setStoryId(int storyId) {
        this.storyId = storyId;
    }

    public boolean isClosed() {
        return closed;
    }

    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }
}
