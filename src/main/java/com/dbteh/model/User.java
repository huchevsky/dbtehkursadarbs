package com.dbteh.model;

/**
 * Created by huch on 10.12.2016.
 */
public class User {
    private String username;
    private String password;
    private int id;
    private String name;
    private String surname;
    private String permissions;
    private String profilePicture;

    public User(String username, String password, int id) {
        this.username = username;
        this.password = password;
        this.id = id;
    }

    public User(String username, String name, String surname,
                String profilePicture) {
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.profilePicture = profilePicture;
    }

    public User(int id, String name, String surname, String profilePicture) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.profilePicture = profilePicture;
    }

    public int getUsrId(){
        return this.id;
    }
    public String getUsrUsername() {
        return this.username;
    }
    public String getUsrPassword() {
        return this.password;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
}
