package com.dbteh.dao;

import com.dbteh.model.User;

/**
 * Created by huch on 10.12.2016.
 */

public interface UserDao {
    public void insert(User usr);
    public void init();
    public User select(int id);
}
