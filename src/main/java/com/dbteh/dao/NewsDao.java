package com.dbteh.dao;

import com.dbteh.model.News;

import java.util.List;

/**
 * Created by huch on 11.12.2016.
 */
public interface NewsDao {
    //public void insert(User usr);
    public void init();
    public List<News> getNewsByPage(int pageId);
    public News select(int id);
    public int getPageCount();
}
