package com.dbteh.dao.impl;

import com.dbteh.dao.UserDao;
import com.dbteh.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;

/**
 * Created by huch on 10.12.2016.
 */
@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    public JdbcTemplate jdbcTemplate;

    @Autowired // Spring will inject JdbcTemplate here
    private JdbcOperations jdbcOperations;

    public void insert(User usr) {
        String sql = "INSERT INTO USER " +
                "(USR_ID, EMAIL, PHONE) VALUES (?, ?, ?)";
        jdbcTemplate.execute(sql);
    }

    public String test() {
        String sql = "SELECT * FROM users";
        jdbcTemplate.execute(sql);
        return "yo";
    }

    public void init() {
        String sql = "CREATE TABLE USER (USR_ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,EMAIL VARCHAR(30) NOT NULL,PHONE VARCHAR(15) NOT NULL)";
        jdbcTemplate.execute(sql);
    }

    @Override
    public User select(int id) {
        return null;
    }
}
