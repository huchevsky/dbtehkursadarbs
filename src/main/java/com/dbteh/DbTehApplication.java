package com.dbteh;

import com.oracle.webservices.internal.api.message.PropertySet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
@PropertySource("classpath:application.properties")
public class DbTehApplication {

	private static final String DATABASE_DRIVER = "spring.datasource.driver-class-name";
	private static final String DATABASE_PASSWORD = "spring.datasource.password";
	private static final String DATABASE_URL = "spring.datasource.url";
	private static final String DATABASE_USERNAME = "spring.datasource.username";

	@Autowired
	private Environment env;

	@Bean
	public DriverManagerDataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		System.out.println(env.getProperty(DATABASE_URL));
		dataSource.setDriverClassName(env.getProperty(DATABASE_DRIVER));
		dataSource.setUrl(env.getProperty(DATABASE_URL));
		dataSource.setUsername(env.getProperty(DATABASE_USERNAME));
		dataSource.setPassword(env.getProperty(DATABASE_PASSWORD));
		return dataSource;
	}

	@Bean
	public JdbcTemplate getTemplate() {
		return new JdbcTemplate(getDataSource());
	}

	public static void main(String[] args) {
		SpringApplication.run(DbTehApplication.class, args);
	}
}
